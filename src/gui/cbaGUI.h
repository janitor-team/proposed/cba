//cbaGUI.h

/*

cbeam_gui    0.3.6    07/2010    G.P.L.

this is a small frontend for the cbeam_class (continuous beam analysis)
cbeam_class has to be included

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef cbeam_gui
#define cbeam_gui

#include <wx/wx.h>
#include <wx/notebook.h>
#include <wx/imaglist.h>
#include <wx/grid.h>
#include <wx/filename.h>
#include <wx/textfile.h>

#include <clocale>

#include "cbeam_class.h"
#include "cbaGUI_dialogs.h"
#include "cbaGUI_drawings.h"
#include "cbaGUI_bitmaps.h"


//--------------- MainApp ---------------

class MainApp: public wxApp
{
    virtual bool OnInit();
    int OnExit();

};


//--------------- MainFrame ---------------
class MainFrame: public wxFrame
{
 public:

	MainFrame(const wxString& title);
    virtual ~MainFrame();
    int Draw(wxDC&, wxRect, int);

    DECLARE_EVENT_TABLE()

 private:

    void CreateGUIControls();

    void OnQuit(wxCommandEvent&);
    void OnInfo(wxCommandEvent&);
    void OnNewBeam(wxCommandEvent&);
    void OnOpenFile(wxCommandEvent&);
    void OnSaveInput(wxCommandEvent&);
    void OnSettings(wxCommandEvent&);
    void SetDefaults(wxArrayString);

    void OnSpanAdd(wxCommandEvent&);
    void OnSpanDel(wxCommandEvent&);
    void OnSetEI(wxCommandEvent&);
    void SetMatSec(wxArrayString);
    void OnSetRst(wxCommandEvent&);

    void OnLoadAdd(wxCommandEvent&);
    void OnLoadDel(wxCommandEvent&);
    void OnSetLF(wxCommandEvent&);
    void OnChkSw(wxCommandEvent&);

    void OnClipRes(wxCommandEvent&);
    void OnSaveRes(wxCommandEvent&);
    void OnPrint(wxCommandEvent&);

    void OnSysPage(wxNotebookEvent&);
    void OnLoadPage(wxNotebookEvent&);
    void OnRadioButton(wxCommandEvent&);
    void OnCellInputGrid(wxGridEvent&);

    void Solve();
    vector< vector<double> > ReadInput();
    void UpdateResultText(wxArrayString, wxArrayString, wxString);
    wxArrayString lmSort(vector< vector<double> >);
    wxArrayString resOut(vector< vector<double> >, vector< vector <double> >) ;
    wxString strOut(vector< vector<double> >);
    wxArrayString CreatePlotTable();

    void FillInputGrids();
    void addCols(wxGrid*, int);
    void addRows(int);
    wxString ChkValue(wxString valStr);

    wxGrid *udl_Grid, *pl_Grid, *pudl_Grid, *ml_Grid, *tz_Grid, *ptr_Grid;
    wxGrid *grid;
    wxNotebook *NbInp, *NbRes;
    wxTextCtrl *TcRes;

    wxButton *BtLdAdd, *BtLdDel, *BtLdFac;
    wxButton *BtEI, *BtRst, *BtSpAdd, *BtSpDel;
    wxCheckBox *ChkSw;

    wxBitmapButton *BtCopy, *BtSave, *BtPrint;
    wxRadioButton *BtFc, *BtSt;

    GraphFrame *system, *graph;


    class Store_values
    {
      public:

        vector<double> L;
        vector<double> R;
        double E,I;
        double Wy,Az;
        double g0;

        vector< vector<double> > LMg;
        vector< vector<double> > LMq;

        wxArrayString resTxt;

        wxString Mat, Sec;
        wxString dbMat, dbSec;
        wxString fcUnit, lnUnit;
        wxString ldFac;

        wxString Head;

    };

    Store_values Store;

    enum
    {
        ID_QUIT = wxID_HIGHEST,
        ID_INFO,
        ID_NEW,
        ID_OPEN,
        ID_SAVE,
        ID_SET,

        ID_NB_INP,
        ID_IGRID,

        ID_BT_LD_ADD,
        ID_BT_LD_DEL,
        ID_BT_LD_FAC,

        ID_BT_EI,
        ID_BT_RST,

        ID_BT_SP_ADD,
        ID_BT_SP_DEL,

        ID_CHK_SW,

        ID_NB_RES,
        ID_TC_RES,

        ID_BT_FC,
        ID_BT_ST,

        ID_BT_COPY,
        ID_BT_SAVE,
        ID_BT_PRINT,

    };

};


//---------------- declare the main frame -------------------------
MainFrame *frame;


//---------------- PrintFrame -------------------------

class PrintFrame: public wxPrintout
{
 public:

  PrintFrame(const wxChar *title = _T("cba")) : wxPrintout(title) {}
  virtual ~PrintFrame() {}

  void GetPageInfo(int *minPage, int *maxPage, int *selPageFrom, int *selPageTo);
  bool HasPage(int page);
  bool OnBeginDocument(int startPage, int endPage);
  bool OnPrintPage(int page);

 private:

  int resNo;

};




#endif

