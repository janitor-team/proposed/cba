//cbaGUI_dialogs.h

#ifndef cbaGUI_dialogs
#define cbaGUI_dialogs

#include <wx/wx.h>
#include <wx/spinctrl.h>
#include <wx/grid.h>
#include <wx/treectrl.h>
#include <wx/listctrl.h>
#include <wx/file.h>
#include <wx/filename.h>
#include <wx/textfile.h>
#include <wx/xml/xml.h>


//---------------- new beam dialog -------------------------
class BeamDialog: public wxDialog
{
 public:

    BeamDialog(wxFrame*, wxWindowID, const wxString&);
    void OnClose(wxCommandEvent&);
    void OnTcCheck(wxCommandEvent&);

    wxArrayString GetItems() { return text; }

    DECLARE_EVENT_TABLE()

 private:

    wxSpinCtrl *TcNum;
    wxArrayString text;

    enum
    {
        ID_BT_OK = wxID_HIGHEST,
        ID_BT_ESC,
    };
};


//---------------- load factor dialog -------------------------
class LoadFactorDialog: public wxDialog
{
 public:

    LoadFactorDialog(wxFrame*, wxWindowID, const wxString&, wxString);
    void OnClose(wxCommandEvent&);
    void OnChkBox(wxCommandEvent&);
    void OnTcCheck(wxCommandEvent&);
    wxString GetFactors() { return text; }

    DECLARE_EVENT_TABLE()

 private:

    wxCheckBox *ChkMin;
    wxBoxSizer *Vsizer;
    wxString text;

    enum
    {
        ID_BT_OK = wxID_HIGHEST,
        ID_BT_ESC,
        ID_CHK,
    };
};


//---------------- constraints dialog -------------------------
class ConstraintsDialog: public wxDialog
{
 public:

    ConstraintsDialog(wxFrame*, wxWindowID, const wxString&, wxArrayString);
    void OnClose(wxCommandEvent&);
    wxArrayString GetValues() { return values; }

    DECLARE_EVENT_TABLE()

 private:

    wxGrid *grid;
    wxArrayString values;

    enum
    {
        ID_BT_OK = wxID_HIGHEST,
        ID_BT_ESC,
    };
};


//---------------- materials/sections dialog -------------------------
class MatSecDialog: public wxDialog
{
 public:

    MatSecDialog(wxFrame*, wxWindowID, const wxString&);

    void SetDbLoc(wxString, wxString);
    void SetUnits(wxString, wxString);
    void SetMat(wxString);
    void SetSec(wxString);
    void SetEI(wxString, wxString);
    wxArrayString GetItems();

    DECLARE_EVENT_TABLE()

 private:

    void OnClose(wxCommandEvent&);
    void OnMatChanged(wxTreeEvent&);
    void OnSecChanged(wxTreeEvent&);
    void OnMatTable(wxListEvent&);
    void OnSecTable(wxListEvent&);
    void OnChkRot(wxCommandEvent&);
    void OnChkHol(wxCommandEvent&);
    void OnSetRes(wxCommandEvent&);
    void OnTcCheck(wxCommandEvent&);

    void SetMatList(wxString);
    void SetSecList(wxString);
    void CopyTableToTc(wxString);
    wxString QueryDB(wxString);
    wxArrayString QueryTable(wxString, wxString, wxString);
    double ConvUnit(wxString, wxString, wxString);
    wxString sMult(wxString, double);

    void ShowDefSec(bool);
    void UpdateSecTc();
    void CalcSec();

    wxTreeCtrl *MatTree, *SecTree;
    wxListCtrl *MatList, *SecList;
    wxBoxSizer *DefSec, *SecSizer, *Vsizer;
    wxCheckBox *ChkRot, *ChkHol;

    wxTreeItemId defId, dbId;
    wxString dbMat, dbSec;
    wxString dbMatUnit, dbSecUnit;
    wxString fcUnit, lnUnit;

    wxArrayString text;

    enum
    {
        ID_BT_OK = wxID_HIGHEST,
        ID_BT_ESC,
        ID_BT_SET,

        ID_MAT_TREE,
        ID_MAT_LIST,
        ID_SEC_TREE,
        ID_SEC_LIST,

        ID_CHK_ROT,
        ID_CHK_HOL,
    };

    //drawing canvas
    class SecGraph: public wxScrolledWindow
    {
     public:
        SecGraph() {};
        virtual ~SecGraph() {};
        void SetType(wxString, bool, bool);
        DECLARE_EVENT_TABLE()

      private:
        void OnPaint(wxPaintEvent&);
        void OnSize(wxSizeEvent&);
        void DrawRect(wxDC&, wxRect, bool, bool);
        void DrawIsec(wxDC&, wxRect, bool);
        void DrawTube(wxDC&, wxRect, bool);
        void DrawDim(wxDC&, wxPoint, int, int, wxString, bool);
        int type;
    };

    SecGraph *secgraph;

};


//---------------- settings dialog -------------------------
class SettingsDialog: public wxDialog
{
 public:

    SettingsDialog(wxFrame*, wxWindowID, const wxString&, wxString);
    wxArrayString GetItems();

    DECLARE_EVENT_TABLE()

 private:

    void OnClose(wxCommandEvent&);
    void OnSetLF(wxCommandEvent&);
    void OnSetDB(wxCommandEvent&);
    void ReadXmlFile(wxString);
    void ChangeXmlFile();
    wxString ChkXML(wxString);
    void CreateDefaultFile(wxString);

    wxCheckBox *ChkHd;
    wxComboBox *CbFc, *CbLn;
    wxTextCtrl *TcHd, *TcMat, *TcSec;

    wxString xmLoc, ldFac;

    enum
    {
        ID_BT_OK = wxID_HIGHEST,
        ID_BT_ESC,
        ID_BT_LF,
        ID_BT_MAT,
        ID_BT_SEC,
        ID_CHK,
    };

};


#endif
