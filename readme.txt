cba 0.3.6	07/2010		G.P.L.
--------------------------------------
calculates member forces and support reactions of a continuous beam

files:

cba		[cba.exe]	binary
cba-gtk [wxCBA.exe]	binary (gui)
materials.xml		material database (gui)
sections.xml		section database (gui)

readme.txt	this file
licence.txt	g.p.l.
/src		source code
/src/gui	source code (gui)

usage:

./cba [cba.exe] -i inputfile [-o outputfile -p plotfile]	
		
-i 	inputfile   	textfile, has to have the following format: 

			SPANS lengths of spans 
			INERTIA moments of inertia per span (optional, default is 1.0)      
			ELASTICITY modulus of elasticity for the beam (optional, default is 1.0)
			CONSTRAINTS constraints def/rot per support (optional, default is -1 0)
				def = support displacement
				rot = support rotation
					-1  supported
					0   not supported
					>0  spring
			LOAD load matrix: span type perm live start len
				span = number of span to be loaded
				load type = 
					1 - uniformly distributed load
					2 - point load
					3 - partial udl
					4 - moment load
					5 - trapezoidal load (c=0 triangular)
					6 - partial triangular load (c<0 ascending, c>0 descending)
				perm = value of permanent load (can be zero)
				live = value of live load (can be zero)     
				start = start for load type 3,5-6, or point for load type 2/4
				len = length for load type 3,5-6 (can be negative for lt6)
			FACTORS load factors perm/live (optional, default is 1.0 1.0 [1.0 0.0]) 
				you can also set the load factors for disburdening loads, 
				this would be e.g. 1.35 1.5 1.35 0.0 
				
-o outputfile 	(optional) - result file, contains the max/min results per span

-p plotfile 	(optional) - result file, contains 7 results along the beam for graphical output:
			
			x, Mmax, Mmin, Vmax, Vmin (with load factors) dmax, dmin (characteristic)
			you can use this data with your favourite spreadsheet application
			or a graph-plotter like gnuplot, this would be for "-p results.dat" and the moments:
			plot "results.dat" using 1:($2*(-1)) title 'Mmax' with lines, \
			     "results.dat" using 1:($3*(-1)) title 'Mmin' with lines

						 
example:	continuous steel beam (hea200), 2 spans (5.8m, 4.2m) 
		permanent load 10.0 kN/m, live load 5.0 kN/m
				
		first create a textfile example.txt:
			SPANS 5.8 4.2               
			INERTIA 3.69e-5 3.69e-5     
			ELASTICITY 2.1e8           
			CONSTRAINTS -1 0 -1 0 -1 0  
			LOAD 1 1 10.0 5.0 0 0      
			LOAD 2 1 10.0 5.0 0 0      
			FACTORS 1.35 1.5  

		then run: cba -i example.txt -o results.txt -p results.dat
				
		2 files should have been created:
			results.txt with max/min results (move on with the design)
			results.dat with results along the beam, which form the graph
						 
	
usage (gui):	

linux: 	./cba-gtk (wxWidgets-2.8 runtime libraries have to be installed)
win32: 	wxCBA.exe (there is no installer, you�ll only need this file and the databases)
		
the small user interface is powered by wxwidgets library and covers almost all of the features of cbeam_class, 
use the commandline tool if you want different moments of inertia, higher precision or batch processing 

on the first run a settings file (settings.xml) will be created in ~./cba
[win32: settings.xml will be created in the folder you run wxCBA.exe in]
you can copy the database files (sections.xml, materials.xml) to this folder,
or store them elsewhere and change the location in settings


compiling:

cmd: g++ cba_main.cpp cbeam_class.cpp -o cba[.exe]
gui: should be easily compiled using an ide that includes wxwidgets library (e.g. code::blocks, wxDev-C++)

there are makefiles provided in /src for linux, 
try make (you will need wxwidgets-dev libraries for the gui)
before you type make install, check if the paths in the makefile fit your needs


credits:

Colin Caprani (www.colincaprani.com) - well done sourcecode cba.m (octave/mathcad - gpl) 
wxWidgets - great framework


changelog:

0.3.1	09/2009  released at sourceforge
0.3.2	09/2009  added output of plot data (gui)
		 added user dialog for creating a new beam (gui)
0.3.3	11/2009  added small graphical output/ printout (gui)
		 removed solve/save button and plot data (gui)
0.3.4	12/2009  added system graph, loadtypes, restraints and load factors (gui)
		 fixed 2 bugs in cbeam_class
0.3.5	02/2010  added material/section database (gui)
0.3.6	07/2010  added two more loadtypes
		 fixed bug in cbeam_class (vertical nodal loads for lt3/4) 


comments:

feel free to write your comments/suggestions to pierrot <idf.dresden@gmx.net>






  